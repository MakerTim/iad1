import map.HashMap;
import map.HashMapLinear;
import map.HashMapSeparate;
import sort.*;
import tree.AVLTreeOO;
import tree.Node;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
@SuppressWarnings("unused")
public class Starter {

	public static void main(String[] args) {


		mapping();

		sorting();
	}

	private static void mapping() {
		HashMap<String> map;
		Function<String, Integer> hashFunction = (string) -> string.toLowerCase().charAt(0) - 'a';
		IntFunction<String[]> newArrayMaker = String[]::new;
		IntFunction<List<String>[]> newMapMaker = List[]::new;

		map = new HashMapLinear<>(hashFunction, newArrayMaker);
		fillMap(map);
		System.out.println(Arrays.toString(map.toArray()));

		map = new HashMapSeparate<>(hashFunction, newMapMaker, newArrayMaker);
		fillMap(map);
		System.out.println(Arrays.toString(map.toArray()));
	}


	private static void fillMap(HashMap<String> map) {
		map.add("Uhype");
		map.add("Peter");
		map.add("LittleGiantPanda");
		map.add("Leonie");
		map.add("Lola");
		map.add("Rick");
		map.add("Sander");
		map.add("Tim");
		map.add("Richard");
	}

	private static void sorting() {
		Sort<Integer> sort;
		Integer[] toSort = {0, 9, 8, 3, 2, 5, 4, 6, 7, 1};
		IntFunction<Integer[]> newArrayMaker = Integer[]::new;
		Comparator<Integer> integerComparator = Integer::compareTo;

		sort = new MonkeySort<>();
		Integer[] sortedMonkey = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new BubbleSort<>();
		Integer[] sortedBubble = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new SelectionSort<>();
		Integer[] sortedSelect = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new QuickSort<>();
		Integer[] sortedQuick = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new QuickerSort<>();
		Integer[] sortedQuicker = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new MergeSort<>();
		Integer[] sortedMerge = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new HeapSort<>();
		Integer[] sortedHeap = sort.sort(toSort, integerComparator, newArrayMaker);

		sort = new DylanSort<>();
		Integer[] sortedDylan = sort.sort(toSort, integerComparator, newArrayMaker);

		System.out.println(Arrays.toString(sortedDylan));
	}

}
