package dijkstra;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Tim Biesenbeek
 */
public class Dijkstra {

	private Graph graph;
	private Set<Node> nodes;
	private Map<Node, Integer> distances = new HashMap<>();

	public Dijkstra(Graph graph, Node from) {
		this.graph = graph;
		this.nodes = new HashSet<>(graph.getNodes());
		if (!graph.getNodes().contains(from)) {
			throw new IllegalArgumentException("Node should be inside the graph!");
		}
		markCalculated(from, 0);
	}

	public Map<Node, Integer> buildMap(Node to) {
		if (!graph.getNodes().contains(to)) {
			throw new IllegalArgumentException("Node should be inside the graph!");
		}

		while (!distances.containsKey(to)) {
			PathDistance pathDistance = firstInLine();
			if (pathDistance.path == null) {
				break;
			}
			markCalculated(pathDistance.path.getTo(), pathDistance.distance);
		}
		return distances;
	}

	private PathDistance firstInLine() {
		Path closestPath = null;
		int closestPathDistance = Integer.MAX_VALUE / 2;

		for (Map.Entry<Node, Integer> distanceEntry : distances.entrySet()) {
			Node node = distanceEntry.getKey();
			int nodeDistance = distanceEntry.getValue();
			Path shortestPath = null;
			int shortestPathDistance = Integer.MAX_VALUE / 2;
			for (Path nodePaths : node.getPaths()) {
				if (distances.containsKey(nodePaths.getTo())) {
					continue;
				}
				if (nodePaths instanceof WeightedPath) {
					WeightedPath nodeWeightedPath = (WeightedPath) nodePaths;
					if (nodeWeightedPath.getWeight() < shortestPathDistance) {
						shortestPath = nodeWeightedPath;
						shortestPathDistance = nodeWeightedPath.getWeight();
					}
				}
			}

			int totalShortestDistance = shortestPathDistance + nodeDistance;
			if (totalShortestDistance < closestPathDistance) {
				closestPath = shortestPath;
				closestPathDistance = totalShortestDistance;
			}
		}
		return new PathDistance(closestPath, closestPathDistance);
	}

	private void markCalculated(Node node, int distance) {
		nodes.remove(node);
		distances.put(node, distance);
	}

	private static class PathDistance {
		private Path path;
		private int distance;

		private PathDistance(Path path, int distance) {
			this.path = path;
			this.distance = distance;
		}

		@Override
		public String toString() {
			return path == null ? "non" : path.toString();
		}
	}
}
