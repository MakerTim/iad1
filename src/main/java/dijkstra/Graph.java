package dijkstra;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Tim Biesenbeek
 */
public class Graph {

	private Set<Node> nodes = new HashSet<>();

	public Node addNode() {
		return addNode(null);
	}

	public Node addNode(String name) {
		Node node = new Node();
		if (name != null) {
			node.setName(name);
		}
		nodes.add(node);
		return node;
	}

	public Set<Node> getNodes() {
		return nodes;
	}

	public Map<Node, Integer> findShortestRoute(Node from, Node to) {
		Dijkstra dijkstra = new Dijkstra(this, from);
		return dijkstra.buildMap(to);
	}
}
