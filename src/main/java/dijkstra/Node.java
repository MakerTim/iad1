package dijkstra;

import java.util.*;

/**
 * @author Tim Biesenbeek
 */
public class Node {

	private String name;

	Node() {
	}

	private Set<Path> paths = new HashSet<>();

	public Path[] addTwoWayNeighbour(Node neighbour, int weight) {
		return addTwoWayNeighbour(neighbour, weight, null);
	}

	public Path[] addTwoWayNeighbour(Node neighbour, int weight, String name) {
		Path[] newPaths = new Path[2];
		newPaths[0] = this.addNeighbour(neighbour, weight, name);
		newPaths[1] = neighbour.addNeighbour(this, weight, name);
		return newPaths;
	}

	public Path addNeighbour(Node neighbour, int weight) {
		return addNeighbour(neighbour, weight, null);
	}

	public Path addNeighbour(Node neighbour, int weight, String name) {
		Path newPath;
		if (weight < 0) {
			newPath = new Path(this, neighbour);
		} else {
			newPath = new WeightedPath(this, neighbour, weight);
		}
		if (name != null) {
			newPath.setName(name);
		}
		paths.add(newPath);
		return newPath;
	}

	public Collection<Path> getPaths() {
		return paths;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPaths(Set<Path> paths) {
		this.paths = paths;
	}

	@Override
	public String toString() {
		return "Node '" + (name != null ? name : "") + "' [" + paths.size() + "]";
	}
}
