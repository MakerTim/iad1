package dijkstra;

/**
 * @author Tim Biesenbeek
 */
public class Path {

	private String name;
	private Node from;
	private Node to;

	public Path(Node from, Node to) {
		this.from = from;
		this.to = to;
	}

	public Node getFrom() {
		return from;
	}

	public void setFrom(Node from) {
		this.from = from;
	}

	public Node getTo() {
		return to;
	}

	public void setTo(Node to) {
		this.to = to;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Path '" + (name != null ? name : "") + "' [" + from.toString() + "->" + to.toString() + "]";
	}
}
