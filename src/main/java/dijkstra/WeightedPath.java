package dijkstra;

/**
 * @author Tim Biesenbeek
 */
public class WeightedPath extends Path {

	private int weight;

	public WeightedPath(Node from, Node to, int weight) {
		super(from, to);
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}
