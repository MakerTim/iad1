package map;

public interface HashMap<T> {

	void add(T item);

	int indexOf(T item);

	default boolean contains(T item) {
		return indexOf(item) >= 0;
	}

	T[] toArray();

}
