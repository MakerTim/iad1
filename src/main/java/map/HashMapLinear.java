package map;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.IntFunction;

public class HashMapLinear<T> implements HashMap<T> {

	private T[] map;
	private int size;
	private Function<T, Integer> hashFunction;
	private IntFunction<T[]> toArray;

	public HashMapLinear(Function<T, Integer> hashFunction, IntFunction<T[]> toArray) {
		this.hashFunction = hashFunction;
		this.toArray = toArray;
		map = toArray.apply(0);
	}

	private void ensureArraySize(int i) {
		if (map.length <= i) {
			map = Arrays.copyOf(map, i + 1);
		}
	}

	public void add(T item) {
		int firstIndex = hashFunction.apply(item);
		ensureArraySize(firstIndex);
		while (map[firstIndex] != null) {
			firstIndex++;
			ensureArraySize(firstIndex);
		}
		size++;
		map[firstIndex] = item;
	}

	public int indexOf(T item) {
		for (int i = hashFunction.apply(item); i < map.length; i++) {
			T toCompare = map[i];
			if (toCompare == item) {
				return i;
			}
			if (toCompare == null) {
				break;
			}
		}
		return -1;
	}

	@Override
	public T[] toArray() {
		T[] array = toArray.apply(size);
		int j = 0;
		for (T item : map) {
			if (item != null) {
				array[j++] = item;
			}
		}
		return array;
	}
}
