package map;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;

public class HashMapSeparate<T> implements HashMap<T> {

	private List<T>[] map;
	private int size;
	private Function<T, Integer> hashFunction;
	private IntFunction<T[]> toArray;
	private IntFunction<List<T>[]> createMap;

	public HashMapSeparate(Function<T, Integer> hashFunction, IntFunction<List<T>[]> createMap, IntFunction<T[]> toArray) {
		this.hashFunction = hashFunction;
		this.toArray = toArray;
		this.createMap = createMap;
		map = createMap.apply(0);
	}

	private void ensureArraySize(int i) {
		if (map.length <= i) {
			map = Arrays.copyOf(map, i + 1);
		}
	}

	public void add(T item) {
		int index = hashFunction.apply(item);
		ensureArraySize(index);
		if (map[index] == null) {
			map[index] = new LinkedList<>();
		}
		map[index].add(item);
		size++;
	}

	public int indexOf(T item) {
		int index = hashFunction.apply(item);
		ensureArraySize(index);
		if (map[index] == null) {
			return -1;
		}
		return map[index].indexOf(item);
	}

	@Override
	public T[] toArray() {
		T[] array = toArray.apply(size);
		int j = 0;
		for (List<T> itemList : map) {
			if (itemList == null) {
				continue;
			}
			for (T item : itemList) {
				array[j++] = item;
			}
		}
		return array;
	}
}
