package sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class BubbleSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		int length = array.length;

		T[] newArray = Arrays.copyOf(array, length);

		for (int i = 0; i < length; i++) {
			boolean swapped = false;
			for (int j = 0; j < length - i - 1; j++) {
				if (comparable.compare(newArray[j], newArray[j + 1]) > 0) {
					T placeholder = newArray[j];
					newArray[j] = newArray[j + 1];
					newArray[j + 1] = placeholder;
					swapped = true;
				}
			}
			if (!swapped) {
				break;
			}
		}
		return newArray;
	}
}
