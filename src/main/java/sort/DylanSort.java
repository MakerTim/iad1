package sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class DylanSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] arrayIn, Comparator<T> comparable, IntFunction<T[]> toArray) {
		T[] array = Arrays.copyOf(arrayIn, arrayIn.length);
		T[] newArray = toArray.apply(array.length);
		for (int i = 0; i < array.length; i++) {
			HeapSort.Heap<T> heap = new HeapSort.Heap<>(array.length, comparable, toArray);
			for (T anArray : array) {
				if (anArray != null) {
					heap.insert(anArray);
				}
			}
			T toRemove = heap.removeRoot();
			newArray[newArray.length - i - 1] = toRemove;
			for (int j = 0; j < array.length; j++) {
				if (array[j] == toRemove) {
					array[j] = null;
					break;
				}
			}
		}
		return newArray;
	}
}
