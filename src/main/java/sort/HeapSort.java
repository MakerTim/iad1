package sort;

import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class HeapSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		Heap<T> heap = new Heap<>(array.length, comparable, toArray);
		for (T object : array) {
			heap.insert(object);
		}
		T[] newArray = toArray.apply(array.length);
		for (int i = 1; i <= newArray.length; i++) {
			newArray[newArray.length - i] = heap.removeRoot();
		}
		return newArray;
	}

	public static class Heap<T> {
		Comparator<T> comparable;
		private T[] objects;
		private int last;

		Heap(int cap, Comparator<T> c, IntFunction<T[]> toArray) {
			objects = toArray.apply(cap + 1);
			comparable = c;
			last = 0;
		}

		int size() {
			return last;
		}

		boolean isEmpty() {
			return size() == 0;
		}

		T root() {
			return objects[1];
		}

		int compare(T t1, T t2) {
			return comparable.compare(t2, t1);
		}

		void insert(T t) {
			objects[++last] = t;
			upBubble();
		}

		T removeRoot() {
			T root = root();
			objects[1] = objects[last];
			last--;
			downBubble();
			return root;
		}

		void upBubble() {
			int i = size();
			while (i > 1) {
				int parent = i / 2;
				if (compare(objects[i], objects[parent]) > 0) {
					break;
				}
				swap(i, parent);
				i = parent;
			}
		}

		void downBubble() {
			int i = 1;
			while (true) {
				int child = i * 2;
				if (child > size()) {
					break;
				}
				if (child + 1 <= size()) {
					child = minOf(child, child + 1);
				}
				if (comparable.compare(objects[i], objects[child]) < 0) {
					break;
				}
				swap(i, child);
				i = child;
			}
		}

		int minOf(int left, int right) {
			if (comparable.compare(objects[left], objects[right]) < 0) {
				return left;
			}
			return right;
		}

		void swap(int i, int j) {
			T placeholder = objects[i];
			objects[i] = objects[j];
			objects[j] = placeholder;
		}
	}
}
