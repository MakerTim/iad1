package sort;

import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class MergeSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		return splitMergeArray(array, comparable, toArray);
	}

	private T[] splitMergeArray(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		// Split
		int leftSize = array.length / 2;
		int rightSize = array.length - leftSize;
		T[] left = toArray.apply(leftSize);
		T[] right = toArray.apply(rightSize);

		System.arraycopy(array, 0, left, 0, leftSize);
		System.arraycopy(array, leftSize, right, 0, rightSize);

		if (left.length > 1) {
			left = splitMergeArray(left, comparable, toArray);
		}
		if (right.length > 1) {
			right = splitMergeArray(right, comparable, toArray);
		}

		// Merge
		int lI = 0;
		int rI = 0;
		T[] newList = toArray.apply(array.length);
		while (lI + rI < newList.length) {
			T placeholder;
			if (rI >= right.length || (lI < left.length && comparable.compare(left[lI], right[rI]) < 0)) {
				placeholder = left[lI];
				lI++;
			} else {
				placeholder = right[rI];
				rI++;
			}
			newList[lI + rI - 1] = placeholder;
		}
		return newList;
	}
}
