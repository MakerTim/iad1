package sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class MonkeySort<T> implements Sort<T> {

	private static Random random = new Random(1241306);

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		boolean sorted = false;
		T[] shuffle = null;
		while (!sorted) {
			sorted = true;
			shuffle = Arrays.copyOf(array, array.length);
			for (int i = 0; i < array.length; i++) {
				int index = random.nextInt(i + 1);
				T placeholder = shuffle[index];
				shuffle[index] = shuffle[i];
				shuffle[i] = placeholder;
			}
			for (int i = 0; i < shuffle.length - 1; i++) {
				int comp = comparable.compare(shuffle[i], shuffle[i + 1]);
				if (comp > 0) {
					sorted = false;
					break;
				}
			}
		}
		return shuffle;
	}
}
