package sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class QuickSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		T[] sorted = toArray.apply(array.length);

		T[] left = toArray.apply(array.length - 1);
		int leftIndex = 0;
		int pivotIndex = array.length / 2; // mag ook laatste
		T pivot = array[pivotIndex];
		T[] right = toArray.apply(array.length - 1);
		int rightIndex = 0;

		for (int i = 0; i < array.length; i++) {
			if (pivotIndex == i) {
				continue;
			}
			if (comparable.compare(array[i], pivot) < 0) {
				left[leftIndex++] = array[i];
			} else {
				right[rightIndex++] = array[i];
			}
		}
		left = Arrays.copyOf(left, leftIndex);
		right = Arrays.copyOf(right, rightIndex);

		if (left.length != 0) {
			left = sort(left, comparable, toArray);
		}
		if (right.length != 0) {
			right = sort(right, comparable, toArray);
		}
		int index = 0;
		for (T leftI : left) {
			sorted[index++] = leftI;
		}
		sorted[index++] = pivot;
		for (T rightI : right) {
			sorted[index++] = rightI;
		}
		return sorted;
	}
}
