package sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class QuickerSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		T[] sorted = Arrays.copyOf(array, array.length);
		quickSort(sorted, 0, sorted.length - 1, comparable);
		return sorted;
	}

	private void quickSort(T[] array, int low, int high, Comparator<T> comparable) {
		if (low < high && low < array.length) {
			int pivot = partition(array, low, high, comparable);
			quickSort(array, low, pivot - 1, comparable);
			quickSort(array, pivot + 1, high, comparable);
		}
	}

	// Laatste element, plaatst m op de gesoordeerde plek
	private int partition(T[] array, int low, int high, Comparator<T> comparable) {
		T pivot = array[high];
		int pivotPos = low;
		for (int j = low; j < high; j++) {
			if (comparable.compare(pivot, array[j]) >= 0) {
				swap(array, pivotPos, j);
				pivotPos++;
			}
		}
		swap(array, pivotPos, high);
		return pivotPos;
	}

	private void swap(T[] array, int x, int y) {
		T placeholder = array[x];
		array[x] = array[y];
		array[y] = placeholder;
	}
}
