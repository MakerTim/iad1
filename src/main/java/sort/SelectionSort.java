package sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public class SelectionSort<T> implements Sort<T> {

	@Override
	public T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray) {
		T[] brokenArray = Arrays.copyOf(array, array.length);
		T[] newArray = toArray.apply(array.length);
		for (int i = 0; i < array.length; i++) {
			int index = i;
			for (int j = i + 1; j < array.length; j++) {
				if (comparable.compare(brokenArray[j], brokenArray[index]) < 0) {
					index = j;
				}
			}
			newArray[i] = brokenArray[index];
			brokenArray[index] = brokenArray[i];
		}

		return newArray;
	}
}
