package sort;

import java.util.Comparator;
import java.util.function.IntFunction;

/**
 * @author Tim Biesenbeek
 */
public interface Sort<T> {

	T[] sort(T[] array, Comparator<T> comparable, IntFunction<T[]> toArray);

}
