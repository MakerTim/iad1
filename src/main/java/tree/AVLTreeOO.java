package tree;

import java.util.Comparator;

public class AVLTreeOO<T> implements Tree<T> {

	protected Node<T> root;
	private Comparator<T> comparator;

	public AVLTreeOO(Comparator<T> comparator) {
		this.comparator = comparator;
	}

	@Override
	public Comparator<T> getComparator() {
		return comparator;
	}

	@Override
	public void add(T item) {
		if (item == null) {
			return;
		}
		if (root == null) {
			root = new Node<>(this, item);
			return;
		}
		root.add(item);
	}

	public void updateRoot(Node<T> root) {
		this.root = root;
	}

	@Override
	public String toString() {
		return root.toString();
	}
}

