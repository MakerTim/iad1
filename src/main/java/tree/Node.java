package tree;

import java.util.Optional;

public class Node<T> {

	private Tree<T> tree;
	private Node<T> parent;
	private Optional<Node<T>> leftChild = Optional.empty();
	private Optional<Node<T>> rightChild = Optional.empty();

	private T item;

	// Root
	public Node(Tree<T> tree, T item) {
		this.item = item;
		this.tree = tree;
	}

	public Node(Tree<T> tree, Node<T> parent, T item) {
		this(tree, item);
		this.parent = parent;
	}

	void add(T item) {
		if (tree.getComparator().compare(this.item, item) > 0) {
			if (leftChild.isPresent()) {
				leftChild.get().add(item);
			} else {
				leftChild = Optional.of(new Node<>(tree, this, item));
			}
		} else {
			if (rightChild.isPresent()) {
				rightChild.get().add(item);
			} else {
				rightChild = Optional.of(new Node<>(tree, this, item));
			}
		}

		int left = leftCount();
		int right = rightCount();
		if (left - right >= 2) {
			leftChild.get().rotateRight();
		} else if (left - right <= -2) {
			rightChild.get().rotateLeft();
		}
	}

	public void setLeftChild(Node<T> leftChild) {
		this.leftChild = Optional.ofNullable(leftChild);
	}

	public void setRightChild(Node<T> rightChild) {
		this.rightChild = Optional.ofNullable(rightChild);
	}

	public Node<T> getLeftChild() {
		return leftChild.orElse(null);
	}

	public Node<T> getRightChild() {
		return rightChild.orElse(null);
	}

	public T getValue() {
		return item;
	}

	protected int leftCount() {
		return leftChild.map(Node::getChildrenCount).orElse(0);
	}

	protected int rightCount() {
		return rightChild.map(Node::getChildrenCount).orElse(0);
	}

	public int getChildrenCount() {
		int left = leftCount();
		int right = rightCount();
		return Math.max(left, right) + 1;
	}

	public void rotateLeft() {
		Node<T> superParent = parent.parent;
		Optional<Node<T>> oldRight = rightChild;
		Optional<Node<T>> oldLeft = leftChild;
		if (oldLeft.isPresent() && !oldRight.isPresent()) {
			Node<T> parent = this.parent;
			Node<T> left = oldLeft.get();
			this.parent.rightChild = oldLeft;
			left.parent = parent;
			this.leftChild = left.rightChild;
			left.rightChild = Optional.of(this);
			this.parent = left;
			this.parent.rotateLeft();
		} else if (oldRight.isPresent()) {
			parent.rightChild = Optional.empty();
			parent.parent = this;
			leftChild = Optional.of(parent);
			parent = superParent;
			if (parent == null) {
				tree.updateRoot(this);
			} else {
				superParent.rightChild = Optional.of(this);
			}
		} else {
			return;
		}
	}

	public void rotateRight() {
		Node<T> superParent = parent.parent;
		Optional<Node<T>> oldLeft = leftChild;
		Optional<Node<T>> oldRight = rightChild;
		if (oldRight.isPresent() && !oldLeft.isPresent()) {
			Node<T> parent = this.parent;
			Node<T> right = oldRight.get();
			this.parent.leftChild = oldRight;
			right.parent = parent;
			this.rightChild = right.leftChild;
			right.leftChild = Optional.of(this);
			this.parent = right;
			this.parent.rotateRight();
		} else if (oldLeft.isPresent()) {
			parent.leftChild = Optional.empty();
			parent.parent = this;
			rightChild = Optional.of(parent);
			parent = superParent;
			if (parent == null) {
				tree.updateRoot(this);
			} else {
				superParent.leftChild = Optional.of(this);
			}
		} else {
			return;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(item);
		builder.append("]");
		builder.append("->");
		builder.append("{");
		leftChild.ifPresent(left -> builder.append(left.toString()));
		builder.append(",");
		rightChild.ifPresent(right -> builder.append(right.toString()));
		builder.append("}");
		return builder.toString();
	}
}