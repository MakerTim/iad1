package tree;

import java.util.Comparator;

public interface Tree<T> {

	void add(T item);

	void updateRoot(Node<T> node);

	Comparator<T> getComparator();

	default void reAdd(Node<T> node) {
		if (node == null) {
			return;
		}
		add(node.getValue());
		Node<T> left = node.getLeftChild();
		Node<T> right = node.getRightChild();
		reAdd(left);
		reAdd(right);
	}

}
