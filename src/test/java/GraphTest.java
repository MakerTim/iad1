import org.junit.Assert;
import org.junit.Test;

import dijkstra.*;

import java.util.Map;

/**
 * @author Tim Biesenbeek
 */
public class GraphTest {

	@Test
	public void testGraphSorting1() { // https://puu.sh/zKZ8g.png
		Graph graph = new Graph();
		Node nodeA = graph.addNode("A");
		Node nodeB = graph.addNode("B");
		Node nodeC = graph.addNode("C");
		Node nodeX = graph.addNode("X");
		Node nodeY = graph.addNode("Y");

		nodeA.addTwoWayNeighbour(nodeC, 2, "pathAC");
		nodeA.addTwoWayNeighbour(nodeB, 1, "pathAB");
		nodeA.addTwoWayNeighbour(nodeX, 4, "pathAX");
		nodeB.addTwoWayNeighbour(nodeX, 1, "pathBX");
		nodeC.addTwoWayNeighbour(nodeX, 3, "pathCX");
		nodeX.addTwoWayNeighbour(nodeY, 1, "pathXY");

		Map<Node, Integer> distances = graph.findShortestRoute(nodeA, nodeY);

		Assert.assertEquals(Integer.valueOf(0), distances.get(nodeA));
		Assert.assertEquals(Integer.valueOf(1), distances.get(nodeB));
		Assert.assertEquals(Integer.valueOf(2), distances.get(nodeC));
		Assert.assertEquals(Integer.valueOf(2), distances.get(nodeX));
		Assert.assertEquals(Integer.valueOf(3), distances.get(nodeY));
	}

	@Test
	public void testGraphSorting2() { // https://puu.sh/zKZgB.png
		Graph graph = new Graph();

		Node nodeA = graph.addNode("A");
		Node nodeB = graph.addNode("B");
		Node nodeC = graph.addNode("C");
		Node nodeE = graph.addNode("E");
		Node nodeF = graph.addNode("F");
		Node nodeG = graph.addNode("G");
		Node nodeH = graph.addNode("H");
		Node nodeI = graph.addNode("I");
		Node nodeJ = graph.addNode("J");
		Node nodeK = graph.addNode("K");
		Node nodeL = graph.addNode("L");
		Node nodeM = graph.addNode("M");
		Node nodeN = graph.addNode("N");

		nodeA.addTwoWayNeighbour(nodeB, 4);
		nodeA.addTwoWayNeighbour(nodeG, 14);
		nodeA.addTwoWayNeighbour(nodeE, 2);
		nodeA.addTwoWayNeighbour(nodeN, 5);
		nodeA.addTwoWayNeighbour(nodeF, 5);
		nodeB.addTwoWayNeighbour(nodeM, 1);
		nodeC.addTwoWayNeighbour(nodeE, 1);
		nodeE.addTwoWayNeighbour(nodeH, 2);
		nodeF.addTwoWayNeighbour(nodeJ, 4);
		nodeF.addTwoWayNeighbour(nodeI, 1);
		nodeF.addTwoWayNeighbour(nodeK, 4);
		nodeG.addTwoWayNeighbour(nodeJ, 3);
		nodeG.addTwoWayNeighbour(nodeL, 1);
		nodeH.addTwoWayNeighbour(nodeL, 4);
		nodeH.addTwoWayNeighbour(nodeN, 1);
		nodeJ.addTwoWayNeighbour(nodeM, 2);
		nodeJ.addTwoWayNeighbour(nodeN, 9);

		{
			Dijkstra dijkstraFromA = new Dijkstra(graph, nodeA);
			Map<Node, Integer> distancesAL = dijkstraFromA.buildMap(nodeL);

			Assert.assertEquals(Integer.valueOf(0), distancesAL.get(nodeA));
			Assert.assertEquals(Integer.valueOf(4), distancesAL.get(nodeB));
			Assert.assertEquals(Integer.valueOf(3), distancesAL.get(nodeC));
			Assert.assertEquals(Integer.valueOf(2), distancesAL.get(nodeE));
			Assert.assertEquals(Integer.valueOf(5), distancesAL.get(nodeF));
			Assert.assertNull(distancesAL.get(nodeG));
			Assert.assertEquals(Integer.valueOf(4), distancesAL.get(nodeH));
			Assert.assertEquals(Integer.valueOf(6), distancesAL.get(nodeI));
			Assert.assertEquals(Integer.valueOf(7), distancesAL.get(nodeJ));
			Assert.assertNull(distancesAL.get(nodeK));
			Assert.assertEquals(Integer.valueOf(8), distancesAL.get(nodeL));
			Assert.assertEquals(Integer.valueOf(5), distancesAL.get(nodeM));
			Assert.assertEquals(Integer.valueOf(5), distancesAL.get(nodeN));

			Map<Node, Integer> distancesAG = dijkstraFromA.buildMap(nodeG);

			Assert.assertEquals(Integer.valueOf(0), distancesAG.get(nodeA));
			Assert.assertEquals(Integer.valueOf(4), distancesAG.get(nodeB));
			Assert.assertEquals(Integer.valueOf(3), distancesAG.get(nodeC));
			Assert.assertEquals(Integer.valueOf(2), distancesAG.get(nodeE));
			Assert.assertEquals(Integer.valueOf(5), distancesAG.get(nodeF));
			Assert.assertEquals(Integer.valueOf(9), distancesAG.get(nodeG));
			Assert.assertEquals(Integer.valueOf(4), distancesAG.get(nodeH));
			Assert.assertEquals(Integer.valueOf(6), distancesAG.get(nodeI));
			Assert.assertEquals(Integer.valueOf(7), distancesAG.get(nodeJ));
			Assert.assertEquals(Integer.valueOf(9), distancesAG.get(nodeK));
			Assert.assertEquals(Integer.valueOf(8), distancesAG.get(nodeL));
			Assert.assertEquals(Integer.valueOf(5), distancesAG.get(nodeM));
			Assert.assertEquals(Integer.valueOf(5), distancesAG.get(nodeN));

			Map<Node, Integer> distancesAK = dijkstraFromA.buildMap(nodeK);

			Assert.assertEquals(Integer.valueOf(0), distancesAK.get(nodeA));
			Assert.assertEquals(Integer.valueOf(4), distancesAK.get(nodeB));
			Assert.assertEquals(Integer.valueOf(3), distancesAK.get(nodeC));
			Assert.assertEquals(Integer.valueOf(2), distancesAK.get(nodeE));
			Assert.assertEquals(Integer.valueOf(5), distancesAK.get(nodeF));
			Assert.assertEquals(Integer.valueOf(9), distancesAK.get(nodeG));
			Assert.assertEquals(Integer.valueOf(4), distancesAK.get(nodeH));
			Assert.assertEquals(Integer.valueOf(6), distancesAK.get(nodeI));
			Assert.assertEquals(Integer.valueOf(7), distancesAK.get(nodeJ));
			Assert.assertEquals(Integer.valueOf(9), distancesAK.get(nodeK));
			Assert.assertEquals(Integer.valueOf(8), distancesAK.get(nodeL));
			Assert.assertEquals(Integer.valueOf(5), distancesAK.get(nodeM));
			Assert.assertEquals(Integer.valueOf(5), distancesAK.get(nodeN));
		}
		{
			Dijkstra dijkstraFromK = new Dijkstra(graph, nodeK);
			Map<Node, Integer> distancesKK = dijkstraFromK.buildMap(nodeK);

			Assert.assertNull(distancesKK.get(nodeA));
			Assert.assertNull(distancesKK.get(nodeB));
			Assert.assertNull(distancesKK.get(nodeC));
			Assert.assertNull(distancesKK.get(nodeE));
			Assert.assertNull(distancesKK.get(nodeF));
			Assert.assertNull(distancesKK.get(nodeG));
			Assert.assertNull(distancesKK.get(nodeH));
			Assert.assertNull(distancesKK.get(nodeI));
			Assert.assertNull(distancesKK.get(nodeJ));
			Assert.assertEquals(Integer.valueOf(0), distancesKK.get(nodeK));
			Assert.assertNull(distancesKK.get(nodeL));
			Assert.assertNull(distancesKK.get(nodeM));
			Assert.assertNull(distancesKK.get(nodeN));

			Map<Node, Integer> distancesKE = dijkstraFromK.buildMap(nodeE);

			Assert.assertEquals(Integer.valueOf(9), distancesKE.get(nodeA));
			Assert.assertEquals(Integer.valueOf(11), distancesKE.get(nodeB));
			Assert.assertNull(distancesKE.get(nodeC));
			Assert.assertEquals(Integer.valueOf(11), distancesKE.get(nodeE));
			Assert.assertEquals(Integer.valueOf(4), distancesKE.get(nodeF));
			Assert.assertEquals(Integer.valueOf(11), distancesKE.get(nodeG));
			Assert.assertNull(distancesKE.get(nodeH));
			Assert.assertEquals(Integer.valueOf(5), distancesKE.get(nodeI));
			Assert.assertEquals(Integer.valueOf(8), distancesKE.get(nodeJ));
			Assert.assertEquals(Integer.valueOf(0), distancesKK.get(nodeK));
			Assert.assertNull(distancesKE.get(nodeL));
			Assert.assertEquals(Integer.valueOf(10), distancesKE.get(nodeM));
			Assert.assertNull(distancesKE.get(nodeN));

			Map<Node, Integer> distancesKN = dijkstraFromK.buildMap(nodeN);

			Assert.assertEquals(Integer.valueOf(9), distancesKN.get(nodeA));
			Assert.assertEquals(Integer.valueOf(11), distancesKN.get(nodeB));
			Assert.assertEquals(Integer.valueOf(12), distancesKN.get(nodeC));
			Assert.assertEquals(Integer.valueOf(11), distancesKN.get(nodeE));
			Assert.assertEquals(Integer.valueOf(4), distancesKN.get(nodeF));
			Assert.assertEquals(Integer.valueOf(11), distancesKN.get(nodeG));
			Assert.assertEquals(Integer.valueOf(13), distancesKN.get(nodeH));
			Assert.assertEquals(Integer.valueOf(5), distancesKN.get(nodeI));
			Assert.assertEquals(Integer.valueOf(8), distancesKN.get(nodeJ));
			Assert.assertEquals(Integer.valueOf(0), distancesKK.get(nodeK));
			Assert.assertEquals(Integer.valueOf(12), distancesKN.get(nodeL));
			Assert.assertEquals(Integer.valueOf(10), distancesKN.get(nodeM));
			Assert.assertEquals(Integer.valueOf(14), distancesKN.get(nodeN));
		}
	}
}
