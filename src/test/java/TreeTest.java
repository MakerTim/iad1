import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import tree.AVLTreeOO;
import tree.Node;

import java.util.Comparator;

public class TreeTest {

	private AVLTreeOOTest<Integer> tree;

	@Before
	public void refreshTree() {
		tree = new AVLTreeOOTest<>(Integer::compare);
	}

	@Test
	public void rotateLeft() {
		tree.add(1);
		tree.add(2);
		tree.add(3);

		Assert.assertEquals(2, tree.getRoot().getChildrenCount());
		Assert.assertEquals(2, tree.getRoot().getValue());
		Assert.assertEquals(1, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(3, tree.getRoot().getRightChild().getValue());
	}

	@Test
	public void rotateRight() {
		tree.add(10);
		tree.add(9);
		tree.add(8);

		Assert.assertEquals(2, tree.getRoot().getChildrenCount());
		Assert.assertEquals(9, tree.getRoot().getValue());
		Assert.assertEquals(8, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(10, tree.getRoot().getRightChild().getValue());
	}

	@Test
	public void rotateRightTwice() {
		tree.add(10);
		tree.add(9);
		tree.add(8);
		tree.add(7);
		tree.add(6);

		Assert.assertEquals(3, tree.getRoot().getChildrenCount());
		Assert.assertEquals(9, tree.getRoot().getValue());
		Assert.assertEquals(7, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(6, tree.getRoot().getLeftChild().getLeftChild().getValue());
		Assert.assertEquals(8, tree.getRoot().getLeftChild().getRightChild().getValue());
		Assert.assertEquals(10, tree.getRoot().getRightChild().getValue());
	}

	@Test
	public void rotateLeftTwice() {
		tree.add(1);
		tree.add(2);
		tree.add(3);
		tree.add(4);
		tree.add(5);

		Assert.assertEquals(3, tree.getRoot().getChildrenCount());
		Assert.assertEquals(2, tree.getRoot().getValue());
		Assert.assertEquals(1, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(4, tree.getRoot().getRightChild().getValue());
		Assert.assertEquals(3, tree.getRoot().getRightChild().getLeftChild().getValue());
		Assert.assertEquals(5, tree.getRoot().getRightChild().getRightChild().getValue());
	}

	@Test
	public void rotateLeftRight() {
		tree.add(5);
		tree.add(4);
		tree.add(6);
		tree.add(3);
		tree.add(7);
		tree.add(2);
		tree.add(8);

		Assert.assertEquals(3, tree.getRoot().getChildrenCount());
		Assert.assertEquals(5, tree.getRoot().getValue());
		Assert.assertEquals(3, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(2, tree.getRoot().getLeftChild().getLeftChild().getValue());
		Assert.assertEquals(4, tree.getRoot().getLeftChild().getRightChild().getValue());
		Assert.assertEquals(7, tree.getRoot().getRightChild().getValue());
		Assert.assertEquals(6, tree.getRoot().getRightChild().getLeftChild().getValue());
		Assert.assertEquals(8, tree.getRoot().getRightChild().getRightChild().getValue());
	}

	@Test
	public void rotateDoubleLeft() {
		tree.add(3);
		tree.add(1);
		tree.add(2);

		Assert.assertEquals(2, tree.getRoot().getChildrenCount());
		Assert.assertEquals(2, tree.getRoot().getValue());
		Assert.assertEquals(1, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(3, tree.getRoot().getRightChild().getValue());
	}

	@Test
	public void rotateDoubleRight() {
		tree.add(8);
		tree.add(10);
		tree.add(9);

		Assert.assertEquals(2, tree.getRoot().getChildrenCount());
		Assert.assertEquals(9, tree.getRoot().getValue());
		Assert.assertEquals(8, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(10, tree.getRoot().getRightChild().getValue());
	}

	@Test
	public void bigTest() {
		tree.add(5);
		tree.add(4);
		tree.add(3);

		Assert.assertEquals(2, tree.getRoot().getChildrenCount());
		Assert.assertEquals(4, tree.getRoot().getValue());
		Assert.assertEquals(3, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(5, tree.getRoot().getRightChild().getValue());

		tree.add(9);

		Assert.assertEquals(3, tree.getRoot().getChildrenCount());
		Assert.assertEquals(4, tree.getRoot().getValue());
		Assert.assertEquals(3, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(5, tree.getRoot().getRightChild().getValue());
		Assert.assertEquals(9, tree.getRoot().getRightChild().getRightChild().getValue());

		tree.add(8);

		Assert.assertEquals(3, tree.getRoot().getChildrenCount());
		Assert.assertEquals(4, tree.getRoot().getValue());
		Assert.assertEquals(3, tree.getRoot().getLeftChild().getValue());
		Assert.assertEquals(8, tree.getRoot().getRightChild().getValue());
		Assert.assertEquals(5, tree.getRoot().getRightChild().getLeftChild().getValue());
		Assert.assertEquals(9, tree.getRoot().getRightChild().getRightChild().getValue());
	}

	public static class AVLTreeOOTest<T> extends AVLTreeOO<T> {

		public AVLTreeOOTest(Comparator<T> comparator) {
			super(comparator);
		}

		public Node getRoot() {
			return root;
		}
	}
}
